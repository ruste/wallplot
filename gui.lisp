(ql:quickload :cltcl)

(defpackage :gui
  (:use :cl :cltcl :plotter-control)
  (:export
    :run))

(in-package :gui)

(defun call-adjust-a (arg)
  (adjust-a (parse-integer arg)))
(defun call-adjust-b (arg)
  (adjust-b (parse-integer arg)))

(defparameter *main*
  #TCL[proc main {} {
	    wm title . "Plotter Control"
	    button .au -text "A up control" -command {
		cltcl::callLisp call-adjust-a -100
	    }
	    button .bu -text "B up control" -command {
		cltcl::callLisp call-adjust-b -100
	    }
	    button .ad -text "A down control" -command {
		cltcl::callLisp call-adjust-a +100
	    }
	    button .bd -text "B down control" -command {
		cltcl::callLisp call-adjust-b +100
	    }
	    pack .au
	    pack .bu
	    pack .ad
	    pack .bd
	    }
	    main])

(defun run ()
  (event-loop *main*))

