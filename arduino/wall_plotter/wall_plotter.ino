// For RAMPS 1.4
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define E_STEP_PIN         26
#define E_DIR_PIN          28
#define E_ENABLE_PIN       24

#define SDPOWER            -1
#define SDSS               53
#define LED_PIN            13

#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       10
#define HEATER_1_PIN       8
#define TEMP_0_PIN          13   // ANALOG NUMBERING
#define TEMP_1_PIN          14   // ANALOG NUMBERING

#include <Servo.h>

const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
// for your motor

int stepCount = 0;         // number of steps the motor has taken
Servo servo;

void setup() {
  // initialize the serial port:
  Serial.begin(9600);
  pinMode(11,OUTPUT);
  servo.attach(11);
  servo.write(0);
 
 //setup x axis
  pinMode(X_STEP_PIN, OUTPUT);
  pinMode(X_DIR_PIN,OUTPUT);
  digitalWrite(X_DIR_PIN,HIGH);
  pinMode(X_ENABLE_PIN,OUTPUT);
  digitalWrite(X_ENABLE_PIN,LOW);

//setup y axis
  pinMode(Y_STEP_PIN, OUTPUT);
  pinMode(Y_DIR_PIN,OUTPUT);
  digitalWrite(Y_DIR_PIN,HIGH);
  pinMode(Y_ENABLE_PIN,OUTPUT);
  digitalWrite(Y_ENABLE_PIN,LOW);

  
}

#define STEPPER_SLEEP 100
int rcvd;

void loop() {

 while(Serial.available()){
  int newrcvd  = Serial.read();
if(newrcvd!=-1)
  rcvd=newrcvd;
 if(rcvd > 0){
  Serial.write(B10000000);
  if(B10 & rcvd){
  if(B01 & rcvd)
     digitalWrite(X_DIR_PIN, LOW);
  // step one step:
  digitalWrite(X_STEP_PIN,HIGH);
  delayMicroseconds(STEPPER_SLEEP);
  digitalWrite(X_STEP_PIN, LOW);
  delayMicroseconds(STEPPER_SLEEP);

  digitalWrite(X_DIR_PIN,HIGH);
  }

  if(B1000 & rcvd){
  if(B0100 & rcvd){
     digitalWrite(Y_DIR_PIN, LOW);
  }
    digitalWrite(Y_STEP_PIN,HIGH);
  delayMicroseconds(STEPPER_SLEEP);
  digitalWrite(Y_STEP_PIN, LOW);
  delayMicroseconds(STEPPER_SLEEP);

  digitalWrite(Y_DIR_PIN,HIGH);
  }
 }
 Serial.write("Got: ");
 Serial.write(rcvd);
 Serial.write("\n");
  }
//  servo.write(90);
 // Serial.print("steps:");
 // Serial.println(stepCount);
//  stepCount++;
//  delay(00);
}
