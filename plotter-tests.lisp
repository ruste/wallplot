(ql:quickload :prove)

(in-package :plotter-control)

(plan 2)


(subtest "Testing Math"

	 (diag "degrees to radians")
	 (is (* 2 pi)
	     (radians 360))
	 (is pi
	     (radians 180))
	 (is (/ pi 2)
	     (radians 90))
	 
	 (diag "vector magnitude")
	 (is 10.0
	     (mag 10 0))
	 (is 10.0
	     (mag 0 10))
	 (is (* 10 (sqrt 2))
	     (mag 10 10))

	 (diag "sign of number")
	 (is t
	     (sgn -1))
	 (is nil
	     (sgn 0))
	 (is nil
	     (sgn 1))

	 (diag "2d dot product")
	 (is 6
	     (dot 2 0 3 0))
	 (is 3
	     (dot 2 3 0 1))
	 (is 2
	     (dot 2 3 1 0))
	 (is 0
	     (dot 1 0 0 1))
	 (is 1
	     (dot 1 0 1 0))

	  )

(defmacro delta (d v e)
  `(< (abs (- ,v ,e)) ,d))

(subtest "Testing Plotter Position Math"

	 (setf *plotter-a* 300)
	 (setf *plotter-w* 400)
	 (setf *plotter-b* 500)
	 (zero-plotter)

	 (diag "plotter-abs-x and plotter-abs-y")
	 (ok (delta 0.001 0 (plotter-abs-x)))
	 (ok (delta 0.001 *plotter-a* (plotter-abs-y)))

	 (is 0.0 (plotter-x))
	 (is 0.0 (plotter-y))

	 (ok (delta 0.001 (car (cons 100 300))
	                  (car (ab-to-xy 316.227 424.264))))

	 (ok (delta 0.001 (cdr (cons 100 300))
	                  (cdr (ab-to-xy 316.227 424.264))))

	 (ok (delta 0.001 100
		          (rel-to-abs-x 100)))
	 (ok (delta 0.001 300
		          (rel-to-abs-y 0)))

	 (diag "moving plotter head and re-testing positions")
	 (setf *plotter-a* 316.227)
	 (setf *plotter-b* 424.264)

	 (ok (delta 0.001 100
		    (plotter-x)))
	 (ok (delta 0.001 0
		    (plotter-y)))

	 (ok (delta 0.001 100
		    (plotter-abs-x)))
	 (ok (delta 0.001 300
		    (plotter-abs-y)))

	 (setf *plotter-a* 200)
	 (setf *plotter-b* 447.2136)

	 (ok (delta 0.001 0
		    (plotter-x)))
	 (ok (delta 0.001 -100
		    (plotter-y)))

	 (ok (delta 0.001 0
		    (plotter-abs-x)))
	 (ok (delta 0.001 200
		    (plotter-abs-y)))

	 )


(finalize)

