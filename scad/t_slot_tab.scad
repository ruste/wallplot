$fn=50;

t_tab_width=11;

module t_tab(tab_size=10){
    tab_thickness=5;
    channel_width=5.26;
    
    rotate([0,0,180])
    union(){


         
        translate([channel_width/2,0,0])
        polygon([[0,0],
                 [(11-channel_width)/2,0],
                 [0,3.5]]);
        mirror([1,0,0])
        translate([channel_width/2,0,0])
        polygon([[0,0],
                 [(11-channel_width)/2,0],
                 [0,3.5]]);

        translate([-channel_width/2,0,0])
        square([channel_width,3.5]);

        translate([-tab_thickness/2,-tab_size,0])
        square([tab_thickness,tab_size]);
    }
}
