$fn=25;
rod_diameter=33.5;

rod_offset=30;
wheel_diameter=40;
wheel_thickness=2;
number_of_spokes=6;
spoke_thickness=1;
rubber_band_diameter=1;
hub_diameter=8;
wheel_width=rod_diameter;
driveshaft_diameter=5;
driveshaft_flat_offset=2;

difference(){
    union(){
            
        difference(){
            //inside wheel cylinder
            cylinder(r=wheel_diameter/2,h=wheel_width,center=true);
            
            difference(){
                //wheel cylinder
                cylinder(r=wheel_diameter/2-wheel_thickness,h=wheel_width+1,center=true);
                
                //inner wall radius
                rotate_extrude()
                    translate([rod_offset-wheel_thickness,0,0])
                        circle(rod_diameter/2);
            }

           
        }
        
        //spokes
        for(a=[0:360/number_of_spokes:360]){
                    rotate([0,0,a])
                        cube([wheel_diameter,1,wheel_width],center=true);
        }
        
        //center hub
        cylinder(r=hub_diameter/2,h=wheel_width,center=true);
        
    }
 //outer wall radius
 rotate_extrude()
        translate([rod_offset,0,0])
                union(){
                    circle(rod_diameter/2);
                    //rubber band grooves
                    for(a=[0:30:360]){
                        rotate([0,0,a])
                        translate([rod_diameter/2,0,0])
                            circle(rubber_band_diameter);
                    }
                }
                
    //stepper shaft hole
    scale([1.25,1.25,1.25])
    difference(){
                cylinder(r=driveshaft_diameter/2,h=wheel_width+1,center=true);
                translate([driveshaft_diameter/2+driveshaft_flat_offset,0,0])
                    cube([driveshaft_diameter,driveshaft_diameter,wheel_width+1],center=true);
            }

}

