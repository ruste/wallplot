$fn=25;

include <round_mount.scad>;
include <holes.scad>;

difference(){
    union(){
        round_mount(h=25,t=4,hd=m4_clearance,brace=false);
        translate([0,1,0])
        cylinder(r=m4_clearance/2+4,h=4);
    }
    
    translate([0,(m4_clearance+4-7)/2,-0.5])
    cylinder(r=m4_clearance/2,h=5);
}