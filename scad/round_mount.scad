include <beam.scad>
include <holes.scad>

module round_mount(hd=10,t=10,h=52,brace=true){
    
md=hd+4;
mo=(md-7)/2;
    
a=[0,0];
b=[0,md-7];
c=[0,mo];
d=[h,0];
e=[h,31];
f=[32/52*h,0];
g=[32/52*h,md-7+((31-md+7)/h)*32/52*h];
    
union(){
    difference(){
        union(){
            //frame
            beam(a,d,w=7,t=4);
            beam(b,e,w=7,t=4);
            
            if(brace){
                beam(f,g,w=7,t=4);
            }
            
            //bearing housing
            translate([0,mo,0])
            cylinder(r=md/2,h=t);
            
            //bolt hole walls
            translate([h,0,0])
            cylinder(r=m4_clearance/2+2,h=4);
            
            translate([h,31,0])
            cylinder(r=m4_clearance/2+2,h=4);
        }
        union(){

            
            //bolt holes
            translate([h,0,-0.5])
            cylinder(r=m4_clearance/2,h=10);
            
            translate([h,31,-0.5])
            cylinder(r=m4_clearance/2,h=10);

        }
    }
        
}

}

//round_mount(h=25);