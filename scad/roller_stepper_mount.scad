$fn=25;

include<beam.scad>;
include<holes.scad>;


difference(){
    union(){
        beam([0,31],w=7,t=4);
        beam([67.5,0],w=7,t=4);
        beam([31,0],[31,31],w=7,t=4);
        beam([0,31],[67.5,31],w=7,t=4);
        
        translate([67.5,0,0])
        cylinder(r=m4_clearance/2+2,h=4);
        translate([67.5,31,0])
        cylinder(r=m4_clearance/2+2,h=4);
    }
    union(){
        
        //stepper mount
        translate([0,0,-0.5])
        cylinder(r=m3_clearance/2,h=4+1);
        translate([0,31,-0.5])
        cylinder(r=m3_clearance/2,h=4+1);
        translate([31,0,-0.5])
        cylinder(r=m3_clearance/2,h=4+1);
        translate([31,31,-0.5])
        cylinder(r=m3_clearance/2,h=4+1);
        
        //frame mount
        translate([67.5,0,-0.5])
        cylinder(r=m4_clearance/2,h=4+1);
        translate([67.5,31,-0.5])
        cylinder(r=m4_clearance/2,h=4+1);
    }
}