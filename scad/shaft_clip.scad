//Notes on p.40 of notebook

$fn=25;


union(){
    //arms of clip
    for(a=[180-acos(2/2.5):-5:15]){
        rotate([0,0,a])
            translate([3.5,0,0])
                cylinder(r=1,h=2);
        rotate([0,0,-a])
            translate([3.5,0,0])
                cylinder(r=1,h=2);
    }

    //flat of clip
    for(y=[-3.5*sin(acos(2/2.5)):0.1:3.5*sin(acos(2/2.5))]){
        translate([-2-1*2/2.5,y,0])
            cylinder(r=1,h=2);
    }
}