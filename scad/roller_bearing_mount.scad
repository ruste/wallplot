$fn=50;

include<beam.scad>;
include<holes.scad>;



module round_mount(hd=10,t=10){
    
md=hd+4;
mo=(md-7)/2;
    
a=[0,0];
b=[0,md-7];
c=[0,mo];
d=[52,0];
e=[52,31];
f=[32,0];
g=[32,md-7+((31-md+7)/52)*32];
    
union(){
    difference(){
        union(){
            //frame
            beam(a,d,w=7,t=4);
            beam(b,e,w=7,t=4);
            beam(f,g,w=7,t=4);
            
            //bearing housing
            translate([0,mo,0])
            cylinder(r=md/2,h=t);
            
            //bolt hole walls
            translate([52,0,0])
            cylinder(r=m4_clearance/2+2,h=4);
            
            translate([52,31,0])
            cylinder(r=m4_clearance/2+2,h=4);
        }
        union(){

            
            //bolt holes
            translate([52,0,-0.5])
            cylinder(r=m4_clearance/2,h=10);
            
            translate([52,31,-0.5])
            cylinder(r=m4_clearance/2,h=10);

        }
    }
        
}

}

difference(){
    
    round_mount(hd=22.5);

    union(){
        //bearing holes
        translate([0,9.75,-0.5])
        cylinder(r=18.5/2,h=11);
        
        translate([0,9.75,1.5])
        cylinder(r=22.5/2,h=10);
    }
}