$fn=50;

include<beam.scad>;
include<holes.scad>;
include<round_mount.scad>;



mirror([0,1,0])
difference(){
    
    round_mount(hd=22.5,t=8.5);

    union(){
        //bearing holes
        translate([0,9.75,-0.5])
        cylinder(r=18.5/2,h=11);
        
        translate([0,9.75,1.5])
        cylinder(r=22.5/2,h=10);
    }
}