$fn=25;

include<beam.scad>;
include<holes.scad>;

difference(){
    union(){
        beam([32,0],w=8,t=4);
        beam([32,31],w=5,t=4);
        beam([0,31],[32,31],w=8,t=4);
    }
    union(){
        translate([0,0,-0.5])
        cylinder(r=m4_clearance/2,h=5);
        translate([32,31,-0.5])
        cylinder(r=m4_clearance/2,h=5);
        translate([0,31,-0.5])
        cylinder(r=m4_clearance/2,h=5);
        translate([32,0,-0.5])
        cylinder(r=m4_clearance/2,h=5);
    }
}
    