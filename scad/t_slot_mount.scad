$fn=50;

include <beam.scad>
include <holes.scad>
include <t_slot_tab.scad>

rod_diameter=33.5;
roller_diameter=27;
stepper_width=31;
mount_width=stepper_width;
beam_thickness=5;
beam_width=8;
tension_bolt_hole_diameter=m4_clearance;
tension_nut_hole_diameter=8.5;

roller_offset=rod_diameter+roller_diameter/2;
stepper_mount_offset=roller_offset-stepper_width/2;

translate([beam_width/2,beam_width/2+2+3.5,11/2-2.5]) //magic
//translate([beam_width/2,11/2-2.5,beam_width/2]) //magic

rotate([0,0,0])
difference(){
    union(){

        translate([mount_width/2,-beam_width/2,beam_thickness/2])
            rotate([-90,0,0])
            cylinder(r=11/2,h=beam_width);
        
        translate([-beam_width/2,-(beam_width/2+2),beam_thickness/2])
            rotate([0,90,0])
            linear_extrude((mount_width-10+beam_width)/2)

            t_tab(tab_size=2+beam_width/2);
        translate([mount_width/2+5,-(beam_width/2+2),beam_thickness/2])
            rotate([0,90,0])
            linear_extrude((mount_width-10+beam_width)/2)

            t_tab(tab_size=2+beam_width/2);

        beam([mount_width,0],[0,0],w=beam_width,t=beam_thickness);

    }

    union(){

        //tension bolt hole
        translate([mount_width/2,-beam_width/2-.5,beam_thickness/2])
            rotate([-90,0,0])
            cylinder(r=tension_bolt_hole_diameter/2,h=beam_width+1);
        
        //tension nut hole
        translate([mount_width/2,-beam_width/2-.5,beam_thickness/2])
            rotate([-90,0,0])
            cylinder(r=tension_nut_hole_diameter/2,h=3.25+.5,$fn=6);
        
        translate([0,0,-0.5])
        cylinder(r=m4_clearance/2,h=beam_thickness+1);
        translate([mount_width,0,-0.5])
        cylinder(r=m4_clearance/2,h=beam_thickness+1);
    }

}