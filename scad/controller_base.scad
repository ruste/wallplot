$fn=50;

include<beam.scad>;

module post(){
    union(){
        cylinder(r=3.3/2,h=5+4+2.5);
        cylinder(r=5/2,h=5+4);
    }
}

a=[0,0];
b=[0,48.35];
c=[83,0];
d=[75.1,48.35];

union(){
        post();
        translate([0,48.35,0])
            post();
        translate([83,0,0])
            post();
        translate([75.1,48.35,0])
            post();
    
        beam(a,b,t=5,w=7);
        beam(a,c,t=5,w=7);
        beam(a,d,t=5,w=7);
        beam(b,c,t=5,w=7);
        beam(c,d,t=5,w=7);
        beam(b,d,t=5,w=7);

}