module beam(b=[5,0],a=[0,0],w=5,t=3){
    hull(){
        translate([a[0],a[1],t/2]) cylinder(r=w/2,h=t,center=true);
        translate([b[0],b[1],t/2]) cylinder(r=w/2,h=t,center=true);
        
    }

}